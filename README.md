# nest-back

## Built on [Nest](https://github.com/nestjs/nest) framework.

## Installation

```bash
$ npm install
```

## Running the app directly on your computer (requires nodejs:12, docker)

```bash
# make sure you have a .env file with the default options.
$ cp dotenv .env

# bring services up (do not use with docker-compose)
$ ./docker_services

# wait for mysql to start for the first time!
# this will use setings located on the .env file

# you can take a peek at the logs with:
$ docker logs mysql_nest -f

# MySQL is ready once you get the message:
# ... [Server] /usr/sbin/mysqld: ready for connections. Version: '8.0.20'  socket: '/var/run/mysqld/mysqld.sock'  port: 3306  MySQL Community Server - GPL.

# then you can run in development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Running the app with docker-compose

This will download and build all the required images and services required by the app.

```bash
# make sure you have a .env file with the default options.
$ cp dotenv .env

# use docker-compose
$ docker-compose up
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov

# test documentation coverage
$ npm run compodoc:coverage

# Test with newman!
$ npm run newman
```

Test it with postman, just download and run!

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/b38110bfb030274f497b#?env%5BNest%20Local%5D=W3sia2V5IjoiYmFzZXVybCIsInZhbHVlIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwiZW5hYmxlZCI6dHJ1ZX0seyJrZXkiOiJhcGlwcmVmaXgiLCJ2YWx1ZSI6Ii9hcGkvdjEiLCJlbmFibGVkIjp0cnVlfSx7ImtleSI6InVzZXJuYW1lIiwidmFsdWUiOiJhZG1pbiIsImVuYWJsZWQiOnRydWV9LHsia2V5IjoicGFzc3dvcmQiLCJ2YWx1ZSI6InBhc3N3b3JkIiwiZW5hYmxlZCI6dHJ1ZX0seyJrZXkiOiJqd3QiLCJ2YWx1ZSI6ImV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUoxYzJWeWJtRnRaU0k2SW1Ga2JXbHVJaXdpYzNWaUlqb3lMQ0p5ZFd4bGN5STZXeUoxYzJWeWMxOTJhV1YzSWl3aWRYTmxjbk5mWTNKbFlYUmxJaXdpZFhObGNuTmZkWEJrWVhSbElpd2lkWE5sY25OZlpHVnNaWFJsSWl3aWNISnZabWxzWlhOZmRtbGxkeUlzSW5CeWIyWnBiR1Z6WDJOeVpXRjBaU0lzSW5CeWIyWnBiR1Z6WDNWd1pHRjBaU0lzSW5CeWIyWnBiR1Z6WDJSbGJHVjBaU0lzSW5KMWJHVnpYM1pwWlhjaUxDSnlkV3hsYzE5amNtVmhkR1VpTENKeWRXeGxjMTkxY0dSaGRHVWlMQ0p5ZFd4bGMxOWtaV3hsZEdVaUxDSnplWE5zYjJkZmRtbGxkeUpkTENKd2NtOW1hV3hsSWpvaVlXUnRhVzRpTENKdFptRlNaWEYxYVhKbFpDSTZabUZzYzJVc0ltMW1ZVkJoYzNObFpDSTZabUZzYzJVc0ltbGhkQ0k2TVRVNU1qSTFNRFU1TWl3aVpYaHdJam94TlRreU1qWTBPVGt5ZlEua2Fta3BRd3A0bVc3UUtDN01PbTBZVlFUVTJ2d3I5R0NkR2dITml5RjRKNCIsImVuYWJsZWQiOnRydWV9XQ==)

## Front-End

This project has a front end in the making, we are using Angular 9 and we will publish it soon right here!

## Contributing

This project has gitlab ci enabled, you must pass the scripts defined in .gitlab-ci.yml:

```
npm run lint
npm run compodoc:coverage
```

Document your code!, yes! don't be a chicken and just document your code, your future self will thank you!

The .md files on the root folder needs work, so... you are welcome to participate!

NOTE: This file will be upgraded on time, so the pipeline requirements will increase as unit and e2e testing is added. For now this is all we have.

## Docs

Once you run compodoc, docs will be visible on http://127.0.0.1:8080

```bash
$ npx compodoc -p tsconfig.json -s
```

## Api help

### Swagger is on!

You can access swagger auto-generated api help on http://localhost:/3000/api/v1/swagger

Your application must be running.
