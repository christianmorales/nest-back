import { PaginationFiltersFilterDTO } from './paginationfiltersfilter.dto';
import { ApiProperty } from '@nestjs/swagger';
/**
 * @ignore
 */
export class PaginationFiltersDTO {
  @ApiProperty()
  field: string;
  @ApiProperty()
  filters: PaginationFiltersFilterDTO[];
}
