import { PaginationSortsDTO } from './paginationsorts.dto';
import { PaginationFiltersDTO } from './paginationfilters.dto';
import { ApiProperty } from '@nestjs/swagger';
/**
 * @ignore
 */
export class PaginationOptionsDTO {
  @ApiProperty()
  pageSize: number;
  @ApiProperty()
  pageNumber: number;
  @ApiProperty()
  filters: PaginationFiltersDTO[];
  @ApiProperty()
  sorts: PaginationSortsDTO[];
}
