/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { IoAdapter } from '@nestjs/platform-socket.io';
import * as redisIoAdapter from 'socket.io-redis';
import { MyLogger } from '../services/logger.service';
import { ConfigService } from '@nestjs/config';
import { ConfigKeys } from '../enum/configkeys.enum';
/**
 * @ignore
 */
export class RedisIoAdapter extends IoAdapter {
  private readonly _configService: ConfigService = new ConfigService();
  private redisUrl = `redis://:${this._configService.get<string>(
    ConfigKeys.REDIS_AUTH,
  )}@${this._configService.get<string>(
    ConfigKeys.REDIS_HOST,
  )}:${this._configService.get<string>(
    ConfigKeys.REDIS_PORT,
  )}/${this._configService.get<string>(ConfigKeys.REDIS_DB)}`;
  private _logger: MyLogger = new MyLogger();
  createIOServer(port: number, options?: any): any {
    const server = super.createIOServer(port, options);
    const redisAdapter = redisIoAdapter(this.redisUrl);
    server.adapter(redisAdapter);

    this._logger.verbose('RedisIoAdapter->createIOServer');
    return server;
  }
}
