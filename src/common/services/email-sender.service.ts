import { MyLogger } from './logger.service';
import { MailerService, ISendMailOptions } from '@nestjs-modules/mailer';
import { ConfigService } from '@nestjs/config';
import { ConfigKeys } from '../enum/configkeys.enum';
import { Injectable } from '@nestjs/common';

/**
 * Servicio para envio de corre, solo enviar lo necesario.
 *
 */
@Injectable()
export class EmailSenderService {
  constructor(
    private readonly _loggerService: MyLogger,
    private readonly _mailerService: MailerService,
    private readonly _configService: ConfigService,
  ) {}

  send(email: ISendMailOptions): boolean {
    const SEND_USER_EMAILS = this._configService.get<boolean>(
      ConfigKeys.SEND_USER_EMAILS,
    );
    const SMTP_USER = this._configService.get<string>(ConfigKeys.SMTP_USER);
    const SMTP_PASSWORD = this._configService.get<string>(
      ConfigKeys.SMTP_PASSWORD,
    );
    const SMTP_DOMAIN = this._configService.get<string>(ConfigKeys.SMTP_DOMAIN);
    const SMTP_FROM_EMAIL = this._configService.get<string>(
      ConfigKeys.SMTP_FROM_EMAIL,
    );

    if (
      SEND_USER_EMAILS &&
      SMTP_USER &&
      SMTP_PASSWORD &&
      SMTP_DOMAIN &&
      SMTP_FROM_EMAIL
    ) {
      this._mailerService.sendMail(email).then((result) => {
        this._loggerService.verbose('Email sent: ' + JSON.stringify(result));
        return true;
      });
    } else {
      this._loggerService.warn(
        'Email service not configured, please check your env vars',
      );
      return false;
    }
  }
}
