import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app/app.module';
import { readFileSync } from 'fs';
import { TypeORMExceptionFilter } from './common/filters/typeorm-exceptions.filter';
import { RedisIoAdapter } from './common/adapters/redis-io.adapter';
import * as Sentry from '@sentry/node';
import { ConfigService } from '@nestjs/config';
import { ConfigKeys } from './common/enum/configkeys.enum';
/**
 * Arrancan!
 */
async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  const configService = app.get(ConfigService);

  //sentry.io
  Sentry.init({ dsn: configService.get<string>(ConfigKeys.SENTRY_DSN) });

  //este cambio es privado
  
  //CORS
  app.enableCors({
    allowedHeaders: ['Content-Type', 'Authorization', 'x-clientid'],
    origin: ['http://localhost:4200', 'http://localhost:3000'],
  });

  //preparar para loadbalancer
  app.set('trust proxy', 1);

  //conectar el websocket a redis
  app.useWebSocketAdapter(new RedisIoAdapter(app));

  //prefijo global para todas las apis, configurado en app.module en los valores config default
  app.setGlobalPrefix(configService.get<string>(ConfigKeys.API_ROUTE));

  //obtener valores del package.json
  const appPackage = JSON.parse(readFileSync('package.json').toString());

  //preparar la generacion de la documentación del api con los valores de json
  const options = new DocumentBuilder()
    .setTitle(appPackage.name)
    .setDescription(appPackage.description)
    .setVersion(appPackage.version)
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);

  //ruta de acceso a la documentación del api
  SwaggerModule.setup(
    configService.get<string>(ConfigKeys.API_ROUTE) +
      '/' +
      configService.get<string>(ConfigKeys.API_SWAGGER),
    app,
    document,
  );

  //filter global para las excepciones del orm
  app.useGlobalFilters(new TypeORMExceptionFilter());

  //vamonos!
  await app.listen(configService.get<string>(ConfigKeys.PORT));
}

bootstrap();
