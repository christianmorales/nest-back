import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserEntity } from '../modules/users/model/user.entity';
import { UsersService } from '../modules/users/users.service';

import * as _ from 'lodash';
import { LoginResponseDTO } from './dto/loginresponse.dto';
import { compare } from 'bcryptjs';
import { JWTPayLoadDTO } from './dto/jwtPayload.dto';
import { LoginIdentityDTO } from './dto/loginIdentity.dto';
/**
 * Service de auth
 */
@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}
  /**
   * Validamos al usuario, vamos por el con el service, y validamos su password.
   * @todo La validación del password debería estar del lado del service, por si cambia la libreria
   * se cambie también en el service.
   *
   * @param username Usuario que viene de post
   * @param pass Password en post
   * @returns null
   */
  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.getUserByUsername(username);

    if (user && compare(user.password, pass)) {
      return _.omit(user, ['password']);
    }
    return null;
  }
  /**
   * Generación del JWT del usuario
   *
   * @param user Usuario que ha iniciado sesión.
   * @returns {LoginResponseDTO} response adecuada conteniendo el token y la identidad del usuario.
   */
  async login(user: UserEntity): Promise<LoginResponseDTO> {
    const userRules = _.map(user.profile.rules, 'value');

    const payload: JWTPayLoadDTO = {
      //meter estos datos en el token, que despues pasan a req.user
      username: user.username,
      uuid: user.uuid,
      sub: user.id,
      rules: userRules,
      profile: user.profile.name,
      mfaRequired: !!user.mfaSecret,
      mfaPassed: false,
    };

    const identity: LoginIdentityDTO = {
      uuid: user.uuid,
      id: user.id,
      username: user.username,
      rules: userRules,
      profile: user.profile.name,
      mfaRequired: !!user.mfaSecret, //aqui insertamos dependiendo si tiene un secret o no...
      mfaPassed: false, //obviamente al inicio no la ha pasado.
    };

    const response: LoginResponseDTO = {
      //se retorna al front
      access_token: this.jwtService.sign(payload),
      identity: identity,
    };

    return response;
  }

  async validateCode(
    code: string,
    user: UserEntity,
  ): Promise<LoginResponseDTO | boolean> {
    //aqui validar el codigo?
    const valid = false;

    if (!code || !code.length || !valid) {
      return false;
    }

    const userRules = _.map(user.profile.rules, 'value');

    const payload: JWTPayLoadDTO = {
      //meter estos datos en el token, que despues pasan a req.user
      username: user.username,
      uuid: user.uuid,
      sub: user.id,
      rules: userRules,
      profile: user.profile.name,
      mfaRequired: true,
      mfaPassed: true,
    };

    const identity: LoginIdentityDTO = {
      uuid: user.uuid,
      id: user.id,
      username: user.username,
      rules: userRules,
      profile: user.profile.name,
      mfaRequired: true,
      mfaPassed: true,
    };

    const response: LoginResponseDTO = {
      //se retorna al front
      access_token: this.jwtService.sign(payload),
      identity: identity,
    };

    return response;
  }
}
