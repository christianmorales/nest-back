import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { intersection } from 'lodash';
/**
 * Guard que usa las rules insertadas por el decorador
 */
@Injectable()
export class RulesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    //obtener las rules de metadata en contexto de quien lo llama (context.getHandler()=controller)
    const rules = this.reflector.get<string[]>('rules', context.getHandler());

    // esta ruta tiene rules, obtener el user del request...
    const request = context.switchToHttp().getRequest();
    const user = request.user;

    if (!rules) {
      //si la ruta no tiene metadata rules
      return true; //pasa
    }

    if (request.user.profile === 'super') {
      console.log('es super');
      return true;
    }

    //si el usuario tiene por lo menos una de las rules en las suyas, pasa
    return !!intersection(rules, user.rules).length;
  }
}
