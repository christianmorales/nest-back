import { ApiProperty } from '@nestjs/swagger';
/**
 * Clase DTO
 */
export class MfaValidationDTO {
  @ApiProperty({ description: 'Codigo generado de 6 digitos.' })
  code: string;
}
