import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
/**
 * @ignore
 */
export class LoginIdentityDTO {
  @ApiProperty()
  @IsInt()
  id: number;

  @ApiProperty()
  @IsString()
  uuid: string;

  @ApiProperty()
  @IsString()
  username: string;

  @ApiProperty({ type: [String] })
  @IsString()
  rules: string[];

  @ApiProperty()
  @IsString()
  profile: string;

  @ApiProperty()
  mfaRequired: boolean;
  @ApiProperty()
  mfaPassed: boolean;
}
