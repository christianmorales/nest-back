import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  CreateDateColumn,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { UserEntity } from '../../users/model/user.entity';
/**
 * @ignore
 */
@Entity('syslog')
export class SyslogEntity {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({
    type: 'varchar',
    length: 50,
    nullable: true,
  })
  route?: string;

  @Column({
    type: 'varchar',
    length: 15,
    nullable: true,
  })
  method?: string;

  @Column({
    type: 'varchar',
    length: 50,
    nullable: true,
  })
  ip?: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  useragent?: string;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @ManyToOne((type) => UserEntity)
  @JoinColumn()
  user?: UserEntity; //la columna profile, es un Profiles

  @CreateDateColumn({ type: 'timestamp', name: 'createdAt' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updatedAt' })
  updatedAt: Date;

  constructor(
    route: string,
    method: string,
    ip: string,
    useragent: string,
    user?: UserEntity,
  ) {
    this.route = route;
    this.method = method;
    this.ip = ip;
    this.useragent = useragent;
    this.user = user;
  }
}
