/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {
  Controller,
  Get,
  UseGuards,
  Param,
  ParseIntPipe,
  Post,
  Body,
} from '@nestjs/common';
import { Rules } from '../../common/decorators/rules.decorator';
import { RulesGuard } from '../../auth/guards/rules/rules.guard';
import { JwtAuthGuard } from '../../auth/guards/jwt/jwt-auth.guard';
import { SyslogService } from './syslog.service';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { SyslogEntity } from './model/syslog.entity';
import { PaginationOptionsDTO } from '../../common/dto/pagination/paginationoptions.dto';
/**
 * Implementado solo con métodos de lectura.
 */
@ApiBearerAuth() //requerido para que el api en swagger nos dé la opcion para automarizarlo
@ApiTags('syslog')
@Controller('syslog')
@UseGuards(JwtAuthGuard, RulesGuard)
export class SyslogController {
  constructor(private _syslogService: SyslogService) {}
  @ApiBearerAuth()
  @Get()
  @Rules('syslog_view')
  async getAll(): Promise<SyslogEntity[]> {
    return await this._syslogService.getAll();
  }
  @ApiBearerAuth()
  @Get(':id')
  @Rules('syslog_view')
  async getById(@Param('id', ParseIntPipe) id: number): Promise<SyslogEntity> {
    return await this._syslogService.getById(id);
  }
  @ApiBearerAuth()
  @Post('paginate')
  async paginate(
    @Body() paginationOptions: PaginationOptionsDTO,
  ): Promise<SyslogEntity[]> {
    return this._syslogService.paginate(paginationOptions);
  }
}
