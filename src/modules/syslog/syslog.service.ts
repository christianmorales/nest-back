import { Injectable } from '@nestjs/common';
import { SyslogEntity } from './model/syslog.entity';
import { SyslogRepository } from './syslog.repository';
import { PaginationOptionsDTO } from '../../common/dto/pagination/paginationoptions.dto';
import { Like, Equal, Between } from 'typeorm';
import * as moment from 'moment';
import { sortBy } from 'lodash';
/**
 * Syslog Service
 */
@Injectable()
export class SyslogService {
  constructor(private _syslogRepository: SyslogRepository) {}

  async getAll(): Promise<SyslogEntity[]> {
    const users: SyslogEntity[] = await this._syslogRepository.getAll();
    return users;
  }

  async getById(id: number): Promise<SyslogEntity> {
    const user: SyslogEntity = await this._syslogRepository.getById(id);
    return user;
  }

  async newSyslog(log: SyslogEntity): Promise<SyslogEntity> {
    const newLog: SyslogEntity = await this._syslogRepository.newSyslog(log);
    return newLog;
  }

  async paginate(
    paginationOptions: PaginationOptionsDTO,
  ): Promise<SyslogEntity[]> {
    const query = {
      relations: ['user', 'user.profile'],
      where: {},
      order: {},
    };

    const filters = [];
    //const search = '';
    if (paginationOptions.filters && paginationOptions.filters.length) {
      paginationOptions.filters.map((filter) => {
        // =>filter {field:'nameColum', filters:[1]}
        // =>filters:[0] -> {term: "sasa"}
        if (filter.filters && filter.filters.length === 1) {
          if (
            filter.filters[0].term &&
            typeof filter.filters[0].term === 'string'
          ) {
            filters.push({ [filter.field]: Like(filter.filters[0].term) });
            //search += `${filter.field} LIKE '%${filter.filters[0].term}%' `;
          } else if (
            typeof filter.filters[0].term === 'boolean' ||
            typeof filter.filters[0].term === 'number'
          ) {
            if (typeof filter.filters[0].term !== 'undefined') {
              filters.push({ [filter.field]: Equal(filter.filters[0].term) });
              //search[filter.field] = filter.filters[0].term;
            }
          }
          // =>filter {field:'nameColum', filters:[2]}
          // =>filters:[0] -> {condition: "gt", placeholder: "Después de", term: Thu Oct 10 2019 00:00:00 GMT-0500 (Central Daylight Time)}
          // =>filters:[1] -> {condition: "gt", placeholder: "Antes de de", term: Tue Oct 15 2019 00:00:00 GMT-0500 (Central Daylight Time)}
        } else if (filter.filters.length === 2 && filter.filters[0].term) {
          const inicio = moment(filter.filters[0].term).format(
            'YYYY-MM-DD 00:00:00',
          );
          const fin = moment(filter.filters[1].term).format(
            'YYYY-MM-DD 23:59:59',
          );
          //search += `l.createdAt BETWEEN CAST('${inicio}' AS DATETIME) AND CAST('${fin}' AS DATETIME)`;
          filters.push({ createdAt: Between(inicio, fin) });
        }
      });
    }

    query.where = filters;

    const orders = {};

    const ordenes =
      paginationOptions.sorts && paginationOptions.sorts.length
        ? sortBy(paginationOptions.sorts, 'priority')
        : [];
    // generar parte de la consulta con el orderby
    if (ordenes && ordenes.length) {
      ordenes.map((o) => {
        orders[o.field] = o.order;
        //sort += `${o.field} ${o.order}`;
      });
    } else {
      // si no trae orden, por default, ordenar por fecha descendiente
      orders['createdAt'] = 'DESC';
      //sort = 'l.createdAt DESC';
    }

    query.order = orders;
    return await this._syslogRepository.find(query);
  }
}
