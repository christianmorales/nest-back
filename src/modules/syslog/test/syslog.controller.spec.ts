import { Test, TestingModule } from '@nestjs/testing';
import { SyslogController } from '../syslog.controller';

describe('Syslog Controller', () => {
  let controller: SyslogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SyslogController],
    }).compile();

    controller = module.get<SyslogController>(SyslogController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
