import { ApiProperty } from '@nestjs/swagger';
import { RuleDTO } from '../../rules/model/rule.dto';
/**
 * @ignore
 */
export class ProfileDTO {
  @ApiProperty()
  id?: number;
  @ApiProperty()
  name: string;
  @ApiProperty()
  rules?: RuleDTO[];
  constructor(id: number, name: string, rules?: RuleDTO[]) {
    this.id = id;
    this.name = name;
    this.rules = rules;
  }
}
