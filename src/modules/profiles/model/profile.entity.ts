import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { RuleEntity } from '../../rules/model/rule.entity';
/**
 * @ignore
 */
@Entity('profiles')
export class ProfileEntity {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({
    unique: true,
    type: 'varchar',
    length: 50,
    nullable: false,
  })
  name: string;

  @Column({
    type: 'boolean',
    default: true,
  })
  active?: boolean;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @ManyToMany((type) => RuleEntity, { cascade: true })
  @JoinTable()
  rules?: RuleEntity[];

  constructor(id: number, name: string, rules?: RuleEntity[]) {
    this.id = id;
    this.name = name;
    this.rules = rules;
  }
}
