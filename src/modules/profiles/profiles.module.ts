import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProfileEntity } from './model/profile.entity';
import { ProfileMapper } from './model/profile.mapper';
import { ProfilesController } from './profiles.controller';
import { ProfilesRepository } from './profiles.repository';
import { ProfilesService } from './profiles.service';
import { MyLogger } from '../../common/services/logger.service';
/**
 * Módulo para profiles
 */
@Module({
  imports: [TypeOrmModule.forFeature([ProfileEntity])],
  controllers: [ProfilesController],
  providers: [ProfileMapper, ProfilesRepository, ProfilesService, MyLogger],
  exports: [ProfileMapper, ProfilesRepository, ProfilesService],
})
export class ProfilesModule {}
