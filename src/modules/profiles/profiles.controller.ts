/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {
  Controller,
  Get,
  UseGuards,
  Param,
  Post,
  Delete,
  Put,
  Body,
  ParseIntPipe,
} from '@nestjs/common';
import { Rules } from '../../common/decorators/rules.decorator';
import { RulesGuard } from '../../auth/guards/rules/rules.guard';
import { JwtAuthGuard } from '../../auth/guards/jwt/jwt-auth.guard';

import { ProfileDTO } from './model/profile.dto';
import { ProfilesService } from './profiles.service';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
/**
 * Controller de profiles
 */
@ApiBearerAuth()
@ApiTags('profiles')
@Controller('profiles')
@UseGuards(JwtAuthGuard, RulesGuard)
export class ProfilesController {
  constructor(private _profilesService: ProfilesService) {}

  @Get('') // /profiles
  @Rules('profiles_view') //establecer las rules a usar en este metodo
  getProfiles(): Promise<ProfileDTO[]> {
    return this._profilesService.getAllProfiles();
  }

  @Get(':id') // /profiles/:id
  @Rules('profiles_view')
  async getProfileById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<ProfileDTO> {
    return await this._profilesService.getProfileById(id);
  }

  @Post()
  @Rules('profiles_create')
  async newProfile(@Body() user: ProfileDTO): Promise<ProfileDTO> {
    return await this._profilesService.newProfile(user);
  }

  @Put(':id')
  @Rules('profiles_update')
  async updateProfile(
    @Param('id') id: number,
    @Body() user: ProfileDTO,
  ): Promise<ProfileDTO> {
    return await this._profilesService.updateProfile(id, user);
  }

  @Delete(':id')
  @Rules('profiles_delete')
  async deleteUser(@Param('id') id: number): Promise<void> {
    return await this._profilesService.deleteProfile(id);
  }
}
