import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RuleEntity } from './model/rule.entity';
import { RuleMapper } from './model/rule.mapper';
import { RulesController } from './rules.controller';
import { RulesRepository } from './rules.repository';
import { RulesService } from './rules.service';
import { MyLogger } from '../../common/services/logger.service';
/**
 * Rules module
 */
@Module({
  imports: [TypeOrmModule.forFeature([RuleEntity])],
  controllers: [RulesController],
  providers: [RulesService, RulesRepository, RuleMapper, MyLogger],
  exports: [RulesService, RulesRepository, RuleMapper],
})
export class RulesModule {}
