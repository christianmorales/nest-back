/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {
  Controller,
  Get,
  UseGuards,
  Post,
  Put,
  Delete,
  Body,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { RulesService } from './rules.service';
import { Rules } from '../../common/decorators/rules.decorator';
import { JwtAuthGuard } from '../../auth/guards/jwt/jwt-auth.guard';
import { RuleDTO } from './model/rule.dto';
import { RulesGuard } from '../../auth/guards/rules/rules.guard';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
/**
 * Rules controller
 */
@ApiBearerAuth()
@ApiTags('rules')
@Controller('rules')
@UseGuards(JwtAuthGuard, RulesGuard)
export class RulesController {
  constructor(private _rulesService: RulesService) {}

  @Get('') // /rules
  @Rules('rules_view')
  getAllRules(): Promise<RuleDTO[]> {
    return this._rulesService.getAllRules();
  }

  @Get(':id') // /rules/:id
  @Rules('rules_view')
  async getRule(@Param('id', ParseIntPipe) id: number): Promise<RuleDTO> {
    return await this._rulesService.getRuleById(id);
  }

  @Post()
  @Rules('rules_create')
  async newRule(@Body() rule: RuleDTO): Promise<RuleDTO> {
    return await this._rulesService.newRule(rule);
  }

  @Put(':id')
  @Rules('rules_update')
  async updateRule(
    @Param('id') id: number,
    @Body() rule: RuleDTO,
  ): Promise<RuleDTO> {
    return await this._rulesService.updateRule(id, rule);
  }

  @Delete(':id')
  @Rules('rules_delete')
  async deleteRule(@Param('id') id: number): Promise<void> {
    return await this._rulesService.deleteRule(id);
  }
}
