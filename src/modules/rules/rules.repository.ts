import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { RuleDTO } from './model/rule.dto';
import { RuleEntity } from './model/rule.entity';
import { RuleMapper } from './model/rule.mapper';
/**
 * Rules Repo
 */
@Injectable()
export class RulesRepository {
  constructor(
    @InjectRepository(RuleEntity)
    private rulesRepository: Repository<RuleEntity>,
    private mapper: RuleMapper,
  ) {}

  getAllRules(): Promise<RuleEntity[]> {
    return this.rulesRepository.find({});
  }

  getRuleById(id: number): Promise<RuleEntity> {
    return this.rulesRepository.findOne(id);
  }

  getRuleByValue(value: string): Promise<RuleEntity> {
    return this.rulesRepository.findOne({ value: value });
  }

  async newRule(ruleDTO: RuleDTO): Promise<RuleEntity> {
    const newRule = this.mapper.dtoToEntity(ruleDTO);
    return this.rulesRepository.save(newRule);
  }

  async updateRule(id: number, ruleDTO: RuleDTO): Promise<RuleEntity> {
    const updateRuleDTO: RuleDTO = new RuleDTO(
      id,
      ruleDTO.name,
      ruleDTO.description,
      ruleDTO.value,
    );
    const updateRule = this.mapper.dtoToEntity(updateRuleDTO);
    await this.rulesRepository.update(id, updateRule);
    return this.rulesRepository.findOne(id);
  }

  deleteRule(id: number): Promise<DeleteResult> {
    return this.rulesRepository.delete(id);
  }
}
