import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
/**
 * @ignore
 */
export class NewMateriasDTO {
  @ApiProperty()
  @IsString()
  nombre: string;
}
