import { Module } from '@nestjs/common';
import { MateriasController } from './materias.controller';
import { MateriasService } from './materias.service';
/**
 * Módulo de materias
 */
@Module({
  controllers: [MateriasController],
  providers: [MateriasService],
})
export class MateriasModule {}
