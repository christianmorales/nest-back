import { Controller, Get, Post, Body, UseGuards } from '@nestjs/common';
import { MateriasDTO } from './dto/materias.dto';
import { MateriasService } from './materias.service';
import { NewMateriasDTO } from './dto/newMaterias.dto';
import { ApiBody, ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from '../../auth/guards/jwt/jwt-auth.guard';
/**
 * Controller de materias
 */
@ApiBearerAuth()
@ApiTags('materias')
@UseGuards(JwtAuthGuard)
@Controller('materias')
export class MateriasController {
  constructor(private readonly _materiasService: MateriasService) {}

  /**
   * Obtener todas las materias
   */
  @Get()
  async get(): Promise<MateriasDTO[]> {
    return this._materiasService.getAll();
  }

  /**
   * Crear nueva materia
   * @param {newMateriasDTO} newMateria  la nueva materia a crear
   */

  @Post()
  @ApiBody({ type: NewMateriasDTO, description: 'Crear una nueva materia' })
  async post(@Body() newMateria: NewMateriasDTO): Promise<MateriasDTO> {
    return this._materiasService.create(newMateria);
  }
}
