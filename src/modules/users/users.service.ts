import { Injectable } from '@nestjs/common';
import { UserEntity } from './model/user.entity';
import { UserDTO } from './model/user.dto';
import { UserMapper } from './model/user.mapper';
import { UsersRepository } from './users.repository';
import * as speakeasy from 'speakeasy';
import * as QRCode from 'qrcode';
import { QrDataDTO } from './model/qrdata.dto';
import { ConfigKeys } from '../../common/enum/configkeys.enum';
import { ConfigService } from '@nestjs/config';
/**
 * Service para usuarios
 */
@Injectable()
export class UsersService {
  constructor(
    private readonly _mapper: UserMapper,
    private readonly usersRepository: UsersRepository,
    private readonly _configService: ConfigService,
  ) {}

  async getAllUsers(): Promise<UserDTO[]> {
    const users: UserEntity[] = await this.usersRepository.getAllUsers();
    return users.map((user) => this._mapper.entityToDto(user));
  }

  async getUserById(id: number): Promise<UserDTO> {
    const user: UserEntity = await this.usersRepository.getUserById(id);
    return this._mapper.entityToDto(user);
  }

  async getUserByUsername(username: string): Promise<UserEntity> {
    return await this.usersRepository.getUserByUsername(username);
  }

  async newUser(userDTO: UserDTO): Promise<UserDTO> {
    const newUser: UserEntity = await this.usersRepository.newUser(userDTO);
    return this._mapper.entityToDto(newUser);
  }

  async updateUser(id: number, userDTO: UserDTO): Promise<UserDTO> {
    const updateUser = await this.usersRepository.updateUser(id, userDTO);
    return this._mapper.entityToDto(updateUser);
  }

  async deleteUser(id: number): Promise<void> {
    await this.usersRepository.deleteUser(id);
  }

  async getUserPicture(id: number): Promise<string> {
    //encargarse que exista el thumb, o generarlo

    return `images/${id}/picture.jpg`;
  }

  async setupMFA(): Promise<QrDataDTO> {
    const secretCode = speakeasy.generateSecret({
      name: this._configService.get<string>(
        ConfigKeys.TWO_FACTOR_AUTHENTICATION_APP_NAME,
      ),
    });
    const qrCode = await QRCode.toDataURL(secretCode.otpauth_url, {
      errorCorrectionLevel: 'H',
    });
    return {
      otpauthUrl: secretCode.otpauth_url,
      base32: secretCode.base32,
      qrCode: qrCode,
    };
  }
}
