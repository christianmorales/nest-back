import { UserDTO } from './user.dto';
import { UserEntity } from './user.entity';
/**
 * @ignore
 */
export class UserMapper {
  dtoToEntity(userDTO: UserDTO): UserEntity {
    return new UserEntity(
      userDTO.id,
      userDTO.uuid,
      userDTO.username,
      userDTO.email,
      userDTO.firstName,
      userDTO.lastName,
      userDTO.password,
      userDTO.active,
      userDTO.rules,
      userDTO.profile,
    );
  }

  entityToDto(userEntity: UserEntity): UserDTO {
    return new UserDTO(
      userEntity.id,
      userEntity.uuid,
      userEntity.username,
      userEntity.email,
      userEntity.firstName,
      userEntity.lastName,
      userEntity.password,
      userEntity.active,
      userEntity.rules,
      userEntity.profile,
    );
  }
}
