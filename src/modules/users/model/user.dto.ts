import { ApiProperty } from '@nestjs/swagger';
import { ProfileDTO } from '../../profiles/model/profile.dto';
/**
 * @ignore
 */
export class UserDTO {
  @ApiProperty()
  readonly id?: number;

  uuid?: string;

  @ApiProperty()
  username: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  firstName: string;
  @ApiProperty()
  lastName: string;
  @ApiProperty()
  password: string;
  @ApiProperty()
  active?: boolean;
  @ApiProperty()
  rules?: string[];
  @ApiProperty({ description: 'Llave para speakeasy' })
  mfaSecret?: string;
  @ApiProperty({ description: 'Fecha de inicialización de MFA' })
  mfaCreated?: string;
  @ApiProperty()
  mfaOTP?: string;
  @ApiProperty({})
  mfaSafeCodes?: string[];
  @ApiProperty()
  profile: ProfileDTO;
  constructor(
    id: number,
    uuid: string,
    username: string,
    email: string,
    firstName: string,
    lastName: string,
    password: string,
    active: boolean,
    rules: string[],
    profile: ProfileDTO,
  ) {
    this.id = id;
    this.uuid = uuid;
    this.username = username;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.password = password;
    this.active = active;
    this.rules = rules;
    this.profile = profile;
  }
}
