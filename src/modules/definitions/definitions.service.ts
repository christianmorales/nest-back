import { Injectable } from '@nestjs/common';
import { CreateDefDTO } from './dto/create-def.dto';
import { Definition } from './interfaces/definition.interface';
import { getRepository } from 'typeorm';
import { DefinitionsEntity } from './definitions.entity';
/**
 * Servicio para las deficiones
 */
@Injectable()
export class DefinitionsService {
  async create(createDefDTO: CreateDefDTO): Promise<Definition> {
    const definitionsRepository = getRepository(DefinitionsEntity);
    const newDefinition: Definition = await definitionsRepository.create(
      createDefDTO,
    );
    return newDefinition;
  }
}
