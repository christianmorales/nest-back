#Usar la imagen oficial, fase y alias: development
FROM node:12.13-alpine As development
#dependencias
RUN apk update && apk add bash
#carpeta de trabajo dentro del container
WORKDIR /nestjs
#copiar a la carpeta de trabajo
COPY package*.json ./
#Instalar solo dependencias de desarrollo (para hacer build)
RUN npm install --only=development
#dependencias adicionales en dev
RUN npm install -g rimraf nc
RUN npm install glob
#Copiamos todo lo requerido
COPY . .
#hacemos build
RUN npm run build

#produccion
FROM node:12.13-alpine as production
#variables
ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}
#misma carpeta
WORKDIR /nestjs
#copiamos todo lo demas
COPY package*.json ./
#instalamos dependencias de produccion
RUN npm install --only=production
#copiamos todo
COPY . .
#copiamos dist que fué generado en la fase de development
COPY --from=development /nestjs/dist ./dist
#arrancar
CMD ["node", "dist/main"]

# How to use: 
# Start mysql and redis...
# $ bash docker_services

# Build the image...
# $ docker build -t nestjs .

# Run with docker...
# $ docker run --network=host -v $PWD/upload:/nestjs/upload --rm --name nestjs nestjs

# Stop the container...
# $ docker stop nestjs